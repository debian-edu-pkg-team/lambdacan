/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <setjmp.h>

#include "lci_platform.h"
#include "lci_ast.h"
#include "lci_var.h"

extern jmp_buf restart_point;

/* Our parser allows variables named A-Z and a-z.  During
 * alpha-conversion, we'll need to be able to come up with new unused
 * variable names.  So we keep track of which variable names have been
 * used with the used_variables array.
 */

#define VAR_ARRAY_SIZE 7
static unsigned char used_variables[VAR_ARRAY_SIZE];  /* one bit for A-Z,a-z */

/* variable_init()
 *
 * in:     nothing
 * out:    used_variables array cleared so all names are unused
 * return: nothing
 *
 * Resets variable namespace state to be ready for a new formula.
 *
 */

void
variable_init(void) {

	int i;

	for(i = 0; i < VAR_ARRAY_SIZE; i++) {
		used_variables[i] = 0;
	}

} /* variable_init() */


/* variable_set_used()
 *
 * in:     var - mark this variable as used
 * out:    used_variables array will be updated to note use of var.
 * return: nothing
 *
 */

void
variable_set_used(variable_t var) {

	unsigned linear_index;  /* A-Z,a-z converted to 0-51 */

	/* First, convert var into a linear index 0-51.  We'll rely on
	 * other parts of interpreter to ensure that var is A-Z or a-z.
	 */
	if(var <= 'Z') {
		linear_index = var - 'A';
	} else {
		linear_index = 1 + ('Z' - 'A') + (var - 'a');
	}

	/* Now, set bit in used_variables[] corresponding to
	 * linear_index.  I'm doing this with a byte array because my
	 * microcontroller-specific compiler doesn't have an 64-bit
	 * integer type.
	 */
	used_variables[(unsigned)(linear_index / 8)] |=
		(0x01 << (linear_index % 8));

} /* variable_set_used() */


/* variable_get_unused()
 *
 * in:     nothing
 * out:    used_variables array will note use of new variable
 * return: new, previously-unused variable
 *
 * Use this function to get a new, previously unused variable for
 * alpha-conversion.
 *
 */

variable_t
variable_get_unused(void) {

	unsigned byte_index, bit_index, bitmask, linear_index;

	for(byte_index = 0; byte_index < VAR_ARRAY_SIZE; byte_index++) {
		for(bit_index = 0; bit_index < 8; bit_index++) {
			bitmask = 0x01 << bit_index;
			if(!(used_variables[byte_index] & bitmask)) {
				used_variables[byte_index] |= bitmask;
				linear_index = byte_index * 8 + bit_index;
				if(linear_index <= 25) {         /* A-Z */
					return 'A' + linear_index;
				} else if(linear_index <= 51) {  /* a-z */
					return 'a' + (linear_index - 26);
				} 
				/* We use only the bottom 52 bits of the
				 * used_variables array.  If the only
				 * free bit is beyond this, we've run
				 * out of variable names.
				 */
			}
		}
	}

	/* If we reach here, we have run out of variable names. */
	NEWLINE;
	PUTS("Out of vars.");
	NEWLINE;
	longjmp(restart_point, -1);

} /* variable_get_unused() */


/* variable_report()
 *
 * in:     nothing
 * out:    nothing
 * return: nothing
 *
 * prints which variables have been used.
 *
 */

void
variable_report(void) {

	unsigned byte_index, bit_index, bitmask, linear_index;

	PUTS("Vars used: ");
	PUTCHAR('\t');
	for(byte_index = 0; byte_index < VAR_ARRAY_SIZE; byte_index++) {
		for(bit_index = 0; bit_index < 8; bit_index++) {
			bitmask = 0x01 << bit_index;
			if(used_variables[byte_index] & bitmask) {
				linear_index = byte_index * 8 + bit_index;
				PUTCHAR((linear_index <= 25 ?
					'A' + linear_index :
					'a' + (linear_index - 26)));
			}
		}
	}
	NEWLINE;

} /* variable_report() */


/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <setjmp.h>

#include "lci_platform.h"
#include "lci_ast.h"
#include "lci_var.h"
#include "lci_parse.h"
#include "lci_reduce.h"

node_t nodes[NODE_ARRAY_LENGTH];
jmp_buf restart_point;     /* program jumps here to restart */


/* common_setup()
 *
 * in:     nothing
 * out:    nothing
 * return: nothing
 *
 * Setup common to all platforms.
 *
 */

static void
common_setup(void) {

	setjmp(restart_point);  /* program jumps here on restart */

} /* common_setup() */


/* common_loop()
 *
 * in:     nothing
 * out:    nothing
 * return: nothing
 *
 * Loop code common to all platforms.
 *
 */

static void
common_loop(void) {

	ast_init();
	variable_init();
	parse_init();
	PUTCHAR('>');
	PUTCHAR('>');
	PUTCHAR(' ');
	parse_formula();
	NEWLINE;
	reduce_formula();
	ast_report();
	variable_report();
	NEWLINE;

} /* common_loop() */


/* Here is the ARDUINO setup and loop functions for use on the
 * microcontroller.
 */
#ifdef ARDUINO

void
setup(void) {
	beginSerial(9600);
	common_setup();
} /* setup() */

void
loop(void) {

	common_loop();

} /* loop() */

#endif


/* Here is a POSIX main function.  Build with POSIX defined to run on
 * a typical POSIX system.  Useful for testing.
 */
#ifdef POSIX

int
main(int argc, char *argv[]) {

	PUTS("LambdaCan version 2.0, Copyright (c) 2005-2008 "
	     "Timothy Jon Fraser");
	PUTS("LambdaCan comes with ABSOLUTELY NO WARRANTY.");
	PUTS("This is free software, and you are welcome to redistribute it");
	PUTS("under certain conditions; see the LICENSE file for details.");
	PUTS("");

	common_setup();

	while(1) {
		common_loop();
	}
	return 0;

} /* main() */

#endif

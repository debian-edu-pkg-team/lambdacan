#ifndef _LCI_PARSE_H_
#define _LCI_PARSE_H_

/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

void parse_init(void);
void parse_formula(void);

#endif

#ifndef _LCI_AST_H_
#define _LCI_AST_H_

/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */


/* This file defines the structures we will use to represent our AST.
 * Our AST will be a traditional tree structure.  We'll handle the
 * allocation of nodes ourselves from a big statically-allocated array
 * of nodes instead of using malloc().  Consequently, the links
 * between nodes are array indexes and not pointers.
 *
 * Our algorithm for managing the array is the linked allocator
 * from volume 1 of Knuth's "The Art of Computer Programming",
 * section 2.2.3.
 *
 */

typedef unsigned char variable_t;  /* for vars and parms */
typedef unsigned char ast_index_t; /* index into ast node array */
typedef enum {
	node_type_unused = 0,
	node_type_variable,
	node_type_application,
	node_type_abstraction
} node_type_t;


/* node_t is the type for nodes in the cell allocator heap.
 * Originally I had a nice struct with three union components to
 * describe the various flavors of node.  It was 3 bytes in size.
 * Unfortunately, the Arduino board's Atmel ATMEGA168 has only 1KB of
 * SRAM, so I need to make nodes even smaller: 2 bytes each.  I
 * thought bitfields would be the answer, but due to word-alignment
 * issues they didn't save any space.  So now I have just an unsigned
 * value with no typed fields and I interpret the bits manually using
 * macros.
 *
 * Field   Usage for each type
 * ------- -----------------------------------------------------------------
 * A: 0-1: All node types:        node_type_t field indicating node type.
 * B: 2-8: node_type_unused:      ast_index_t to next unused node.
 *         node_type_variable:    variable_t field indicating variable name.
 *         node_type_application: ast_index_t to abstraction.
 *         node_type_abstraction: variable_t formal parameter name.
 * C: 9-F: node_type_unused:      unused.
 *         node_type_variable:    unused.
 *         node_type_application: ast_index_t to application argument.
 *         node_type_abstraction: ast_index_t to abstraction body.
 *
 * The first few groups of macros defines a way to get/set each field.
 * The last group uses the earlier ones while providing descriptive
 * names.
 */

typedef unsigned short node_t;  /* see lci_ast.c for explanation */

#define NODE_MASK_TWO   0x0003  /* first field is 2 bits wide */
#define NODE_MASK_SEVEN 0x007F  /* second and third fields are 7 bits wide */
#define NODE_SHIFT_A    0       /* shift for first field  */
#define NODE_SHIFT_B    2       /* shift for second field */
#define NODE_SHIFT_C    9       /* shift for third field  */
#define NODE_GET(n, s, m)    (((n) >> (s)) & (m))
#define NODE_SET(n, s, m, v) ((n) = ((n) & ~((m) << (s))) | \
                                    (((v) & (m)) << (s)))

#define NODE_GET_A(n)    NODE_GET(n, NODE_SHIFT_A, NODE_MASK_TWO)
#define NODE_SET_A(n, v) NODE_SET(n, NODE_SHIFT_A, NODE_MASK_TWO, v)
#define NODE_GET_B(n)    NODE_GET(n, NODE_SHIFT_B, NODE_MASK_SEVEN)
#define NODE_SET_B(n, v) NODE_SET(n, NODE_SHIFT_B, NODE_MASK_SEVEN, v)
#define NODE_GET_C(n)    NODE_GET(n, NODE_SHIFT_C, NODE_MASK_SEVEN)
#define NODE_SET_C(n, v) NODE_SET(n, NODE_SHIFT_C, NODE_MASK_SEVEN, v)

#define NODE_GET_TYPE(n)     NODE_GET_A(n)
#define NODE_SET_TYPE(n, v)  NODE_SET_A(n, v)
#define NODE_GET_LEFT(n)     NODE_GET_B(n)
#define NODE_SET_LEFT(n, v)  NODE_SET_B(n, v)
#define NODE_GET_RIGHT(n)    NODE_GET_C(n)
#define NODE_SET_RIGHT(n, v) NODE_SET_C(n, v)
#define NODE_GET_NEXT(n)     NODE_GET_B(n)
#define NODE_SET_NEXT(n, v)  NODE_SET_B(n, v)
#define NODE_GET_VAR(n)      NODE_GET_B(n)
#define NODE_SET_VAR(n, v)   NODE_SET_B(n, v)
#define NODE_GET_PARM(n)     NODE_GET_B(n)
#define NODE_SET_PARM(n, v)  NODE_SET_B(n, v)
#define NODE_GET_BODY(n)     NODE_GET_C(n)
#define NODE_SET_BODY(n, v)  NODE_SET_C(n, v)

/* Our present target, the Arduino board with an Atmel ATMEGA168 MCU,
 * has only 1KB of SRAM.  That isn't much room, so we're using a
 * linked allocator with only 127 nodes, indexed 0..126.  We'll use
 * 127 for our distinguished DELTA value.
 */
#define NODE_ARRAY_LENGTH 127
#define DELTA NODE_ARRAY_LENGTH

void ast_init(void);
ast_index_t node_new(node_type_t);
void node_free(ast_index_t index);
void ast_print(ast_index_t);
void ast_report(void);

#endif

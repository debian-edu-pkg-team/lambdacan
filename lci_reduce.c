/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lci_platform.h"
#include "lci_ast.h"
#include "lci_var.h"
#include "lci_reduce.h"

extern node_t nodes[];


/* deep_copy()
 *
 * in:     dst   - index of the destination node in the nodes array
 *         src   - index of the source node in the nodes array
 * out:    nodes - nodes[dst] subtree will be made a copy of nodes[src] tree
 * return: nothing
 *
 * To call:  nodes[dst] must be an already-allocated node in the nodes
 * array.  You can allocate a new nodes[dst] just for this call, or 
 * you can reuse an already-allocated node.  Either way, this function
 * will make nodes[dst] a copy of nodes[src], and will then recursively
 * copy all of nodes[src]'s children.  This function allocated new nodes
 * for these children - so at most, only the root nodes[dst] will be
 * reused.
 *
 * Reuse the root nodes[dst] node when you're reducing an application.
 * You can copy what will eventually be the result of the application
 * over the old application node, so whatever parent node once pointed
 * to the pre-reduced application will now automatically point to its
 * result.
 */

static void
deep_copy(ast_index_t dst, ast_index_t src) {

	NODE_SET_TYPE(nodes[dst], NODE_GET_TYPE(nodes[src]));
	switch(NODE_GET_TYPE(nodes[src])) {
	case node_type_application:
		NODE_SET_LEFT(nodes[dst], node_new(node_type_unused));
		deep_copy(NODE_GET_LEFT(nodes[dst]),
			NODE_GET_LEFT(nodes[src]));
		NODE_SET_RIGHT(nodes[dst], node_new(node_type_unused));
		deep_copy(NODE_GET_RIGHT(nodes[dst]),
			NODE_GET_RIGHT(nodes[src]));
		break;
	case node_type_abstraction:
		NODE_SET_PARM(nodes[dst], NODE_GET_PARM(nodes[src]));
		NODE_SET_BODY(nodes[dst], node_new(node_type_unused));
		deep_copy(NODE_GET_BODY(nodes[dst]),
			NODE_GET_BODY(nodes[src]));
		break;
	case node_type_variable:
	default:
		NODE_SET_VAR(nodes[dst], NODE_GET_VAR(nodes[src]));
		break;
	} /* switch on node type */

} /* deep_copy() */


/* deep_free()
 *
 * in:     index - index of subtree to free
 * out:    nodes - nodes[index] subtree will be marked node_type_unused
 * return: nothing
 *
 */

static void
deep_free(ast_index_t index) {

	switch(NODE_GET_TYPE(nodes[index])) {
	case node_type_application:
		deep_free(NODE_GET_LEFT(nodes[index]));
		deep_free(NODE_GET_RIGHT(nodes[index]));
		break;
	case node_type_abstraction:
		deep_free(NODE_GET_BODY(nodes[index]));
		break;
	case node_type_variable:
	default:
		break;
	} /* switch on node type */

	node_free(index);

} /* deep_free() */


/* will_capture()
 *
 * in:     captureparm - parm of lambda being changed by substitution.
 *         index       - index of formula being substituted into the 
 *                       body of the lambda.
 * out:    nothing
 * return: value   condition
 *         -----   ---------
 *         true    captureparm is among the free variables of the formula
 *                 rooted at nodes[index].
 *         false   otherwise.
 *
 * To use this predicate to determine whether or not the substitution
 * "[x->s](\y.b)" would result in variable capture (that is, to check
 * if y is among the free variables of s), call will_capture(y,s).
 *
 */

static int
will_capture(variable_t captureparm, ast_index_t index) {

	switch(NODE_GET_TYPE(nodes[index])) {
	case node_type_application:
		return(will_capture(captureparm, NODE_GET_LEFT(nodes[index]))
			|| will_capture(captureparm,
			NODE_GET_RIGHT(nodes[index])));
		break;
	case node_type_abstraction:
		if(NODE_GET_PARM(nodes[index]) == captureparm) {
			return 0;
		}
		return will_capture(captureparm, NODE_GET_BODY(nodes[index]));
		break;
	case node_type_variable:
	default:
		if(NODE_GET_VAR(nodes[index]) == captureparm) {
			return 1;
		} else {
			return 0;
		}
		break;
	} /* switch on node type */

} /* will_capture() */


/* alpha_convert()
 *
 * in:     old - variable to replace with new
 *         new - variable that will replace old
 *         index - perform replacement on this subformula
 * out:    nodes - subformula rooted at index will have all free instances
 *                 of variable old replaced with new
 * return: nothing
 *
 * Use this function to perform alpha-conversion.
 *
 */

static void
alpha_convert(variable_t old, variable_t nu, ast_index_t index) {

	switch(NODE_GET_TYPE(nodes[index])) {
	case node_type_application:
		alpha_convert(old, nu, NODE_GET_LEFT(nodes[index]));
		alpha_convert(old, nu, NODE_GET_RIGHT(nodes[index]));
		break;
	case node_type_abstraction:
		if(NODE_GET_PARM(nodes[index]) == old) {
			NODE_SET_PARM(nodes[index], nu);
		}
		alpha_convert(old, nu, NODE_GET_BODY(nodes[index]));
		break;
	case node_type_variable:
	default:
		if(NODE_GET_VAR(nodes[index]) == old) {
			NODE_SET_VAR(nodes[index], nu);
		}
		break;
	} /* switch on node type */

} /* alpha_convert() */


/* substitute()
 *
 * in:     index - index of subformula on which to perform substitution
 *         var   - variable to replace
 *         with  - replace var with formula at this index
 * out:    nodes - nodes[index] subformula will have substitution done on it
 * return: nothing
 *
 * Use this function to perform substitution.
 * To do [v->w]i, call substitute(i,v,w)
 */

static void
substitute(ast_index_t index, variable_t var, ast_index_t with) {

	switch(NODE_GET_TYPE(nodes[index])) {
	case node_type_application:
		substitute(NODE_GET_LEFT(nodes[index]), var, with);
		substitute(NODE_GET_RIGHT(nodes[index]), var, with);
		break;
	case node_type_abstraction:
		if(NODE_GET_PARM(nodes[index]) != var) {
			if(will_capture(NODE_GET_PARM(nodes[index]), with)) {
				alpha_convert(NODE_GET_PARM(nodes[index]),
					variable_get_unused(), index);
			}

			substitute(NODE_GET_BODY(nodes[index]), var, with);
		}
		break;
	case node_type_variable:
	default:
		if(NODE_GET_VAR(nodes[index]) == var) {
			deep_copy(index, with);
		}
		break;
	} /* switch on node type */
	
} /* substitute() */


/* beta_reduce(void)
 *
 * in:     nothing
 * out:    formula in global nodes array will have zero or one redex
 *         beta-reduced.  (Most iterations will have one, the last will
 *         have zero.)
 * return: value    condition
 *         -----    ---------
 *           1      more beta-reductions may be needed
 *           0      the formula cannot be reduced further
 *
 * Use this function to perform one step of beta-reduction on the
 * formula rooted at nodes[0].  This function performs full
 * beta-reduction: any redex is a candidate for reduction.  This
 * function always chooses the leftmost redex in the nodes array for
 * reduction.  For large formulas, it's difficult to say which redex
 * this corresponds to in the formula, as garbage collection makes the
 * storage locations of redexes difficult to predict.
 *
 */

static int
beta_reduce(void) {

	unsigned index_app; /* need full unsigned type to detect < array len */
	ast_index_t index_abs;  /* left  side of index_app */
	ast_index_t index_arg;  /* right side of index_app */

	/* Make index point to the first node in the nodes array that
	 * is an application whose left side is an abstraction.
	 */
	for(index_app = 0; index_app < NODE_ARRAY_LENGTH; index_app++) {
		if(NODE_GET_TYPE(nodes[index_app]) == node_type_application) {
			index_abs = NODE_GET_LEFT(nodes[index_app]);
			if(NODE_GET_TYPE(nodes[index_abs]) ==
				node_type_abstraction) {
				break;
			}
		}
	}
	if(index_app >= NODE_ARRAY_LENGTH) {
		return 0;   /* no more reduction possible.  Done */
	}

	/* Make nodes[index_app] a deep copy of the body of nodes[index_abs] */
	index_arg = NODE_GET_RIGHT(nodes[index_app]);  /* for later */
	deep_copy(index_app, NODE_GET_BODY(nodes[index_abs]));

	/* Perform substitution on the new copy of the body */
	substitute(index_app, NODE_GET_PARM(nodes[index_abs]), index_arg);

	/* Deep free abstraction and its argument */
	deep_free(index_abs);
	deep_free(index_arg);

	return 1;   /* further reduction may be possible */

} /* beta_reduce() */


/* reduce_formula()
 *
 * in:     nothing
 * out:    nodes - formula in nodes will be reduced to an irreducible form
 * return: nothing
 *
 * This function iteratively performs one step of beta-reduction at a time,
 * printing intermediate results, until the formula in nodes cannot be
 * further reduced.
 *
 */

void
reduce_formula(void) {

	while(beta_reduce()) {
		PUTCHAR('-');
		PUTCHAR('>');
		PUTCHAR(' ');
		ast_print(0);
		NEWLINE;
	}
	PUTS("/>");

} /* reduce_formula() */


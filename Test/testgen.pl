#!/usr/bin/perl
use strict;

for (my $i = 0; $i < 1000; $i++) {
    &formula;
    print ";;\n";
}

sub formula {

    my $r = rand;

    if ($r < 0.4) {
	&variable;
	return;
    }

    $r = rand;
    if ($r < 0.5) {
	print "(";
	&formula;
	print " ";
	&formula;
	print ")";
    } else {
	print "(\\";
	&variable;
	print ".";
	&formula;
	print ")";
    }

} # formula


sub variable {

    my $v = 97;
    while($v <= 122) {
	my $r = rand;
	if ($r < 0.45) {
	    last;
	}
	$v++;
    }

    print chr($v);

} # variable

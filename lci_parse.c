/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <setjmp.h>

#include "lci_platform.h"
#include "lci_ast.h"
#include "lci_var.h"

extern node_t nodes[];
extern jmp_buf restart_point;

static char token  = 0;    /* we need signed chars so we can represent EOF */
static unsigned int  column = 0;
static unsigned int  line   = 1;

/* parse_init()
 *
 * in:     nothing
 * out:    token   - zeroed
 *         column  - reset to 0 so the first token read will  be column 1
 *         line    - reset to 1 so the first line read will be line 1
 * return: nothing
 *
 * Resets parser state to be ready for a new formula.
 *
 */

void
parse_init(void) {

	token = 0;
	column = 0;
	line = 1;

} /* parse_init() */


/* read_token()
 *
 * in:     nothing
 * out:    token   - will contain next non-whitespace char in input stream
 *         column  - will contain column of char in token
 *         line    - will contain line of char in token
 * return: nothing
 *
 * Reads the next input character, skipping whitespace.  Keeps track of
 * line and column for error reporting.  Line 1 is the first line.
 * Column 1 is the leftmost column.
 *
 */
static void
read_token(void) {

	while(1) {                   /* loop until non-whitespace token */
		token = GETCHAR();
		switch(token) {
		case '\n':           /* new line, and back to column zero */
			line++;
		case '\r':           /* carriage return, back to column zero */
			column = 0;
			continue;    /* loop back to get another token */
		case '\t':           /* tab and space increment column */
		case ' ':
			column++;
			continue;    /* loop back to get another token */
		default:             /* non-whitespace token -- done! */
			column++;
			break;
		} /* switch(token) */
		break;               /* got a non-ws token, end loop */
	} /* while(1) */

} /* read_token() */


/* parser_exit()
 *
 * in:     expected - string describing what kind of token parser expected
 * out:    global program reset
 * return: nothing
 *
 * Reports parse error, triggers global program reset.
 */

static void
parser_exit(const char *expected) {

	NEWLINE;
	PUTS("Parse error.");
	PUTS("L:");
	PUTCHAR('\t');
	PRINT_UINT(line);
	NEWLINE;
	PUTS("C:");
	PUTCHAR('\t');
	PRINT_UINT(column);
	NEWLINE;
	PUTS("Found:");
	PUTCHAR('\t');
	PUTCHAR(token);
	NEWLINE;
	PUTS("Expected:");
	PUTCHAR('\t');
	PUTS(expected);
	NEWLINE;
	longjmp(restart_point, -1);

} /* parser_exit() */


#define ISVAR(t)(((t >= 'a')&&(t <= 'z'))||((t >= 'A')&&(t <= 'Z')))


/* read_variable_or_oparen()
 *
 * in:     nothing
 * out:    calls read_token() - modifies input stream
 * return: nothing
 *
 * Attempts to read a variable or open paren.  This is a function,
 * rather than a macro, only because it is called in more than one
 * place.
 *
 */

static void
read_variable_or_oparen(void) {

	read_token();
	if(!((ISVAR(token)) || (token == '('))) {
		parser_exit("var or (");
	}

} /* read_variable_or_oparen() */


#define READ_VARIABLE_LAMBDA_OR_OPAREN \
	read_token();   \
	if(!((ISVAR(token)) || (token == '\\') || (token == '('))) {  \
		parser_exit("var, \\, or (");  \
	}

#define READ_VARIABLE \
	read_token();   \
	if(!(ISVAR(token))) {  \
		parser_exit("var");  \
	}

#define READ_PERIOD \
	read_token();   \
	if(!(token == '.')) {  \
		parser_exit(".");  \
	}

#define READ_CPAREN \
	read_token();   \
	if(!(token == ')')) {  \
		parser_exit(")");  \
	}

#define READ_SEMI \
	read_token();   \
	if(!(token == ';')) {  \
		parser_exit(";");  \
	}


/* parse_subformula()
 *
 * in:     nothing
 * out:    calls read_token() - modifies input stream
 *         calls node_new and various set functions - modifies nodes array
 * return: index of top node of parsed (sub-)formula 
 *
 * Use this function to recursively parse the input stream into an AST
 * in the global nodes array.  Because of the way node allocation (and
 * later, evaluation) works, node 0 is always the root of the entire
 * formula.
 *
 * To begin parsing, call parse_formula() rather than this function.
 */

static ast_index_t
parse_subformula(void) {

	ast_index_t index;                /* index of node for this formula */

	switch(token) {
	case '(':                             /* application or abstraction */
		READ_VARIABLE_LAMBDA_OR_OPAREN;  /* lambda or app left side */
		switch(token) {
		case '\\':                                   /* abstraction */
			index = node_new(node_type_abstraction);
			READ_VARIABLE;                         /* parameter */
			NODE_SET_PARM(nodes[index], token);
			variable_set_used(token);
			READ_PERIOD;                              /* period */
			read_variable_or_oparen();                  /* body */
			NODE_SET_BODY(nodes[index], parse_subformula());
			break;
		default:                                     /* application */
			index = node_new(node_type_application);
			NODE_SET_LEFT(nodes[index], parse_subformula());
			read_variable_or_oparen();        /* app right side */
			NODE_SET_RIGHT(nodes[index], parse_subformula());
			break;
		}
		READ_CPAREN;                /* close paren for this formula */
		break;
	default:                                                /* variable */
		index = node_new(node_type_variable);
		NODE_SET_VAR(nodes[index], token);
		variable_set_used(token);
		break;
	}

	return index;     /* return index of node representing this formula */

} /* parse_subformula() */


/* parse_formula()
 *
 * in:     nothing
 * out:    calls read_*, parse_subformula - modifies input stream, nodes array
 * return: nothing
 *
 * Parses the formula described by input stream using parse_subformula() as
 * a helper.
 */

void
parse_formula(void) {

	read_variable_or_oparen();
	parse_subformula();
	READ_SEMI;
	READ_SEMI;

} /* parse_formula() */

/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <setjmp.h>

#include "lci_platform.h"
#include "lci_ast.h"

extern node_t nodes[];
extern jmp_buf restart_point;

static ast_index_t node_alloc_next = 0;
static ast_index_t current_nodes_allocated = 0;
static ast_index_t node_high_watermark = 0;

/* ast_init()
 *
 * in:     nothing
 * out:    node_alloc_next         - zeroed so next node alloc'd will be 0
 *         current_nodes_allocated - zeroed
 *         node_high_watermark     - zeroed
 *         nodes                   - all nodes set to unused
 * return: nothing
 *
 * Resets state of AST to empty, ready to accept a new formula.
 *
 */

void
ast_init(void) {

	unsigned index;  /* need > 8 bits so loop won't roll forever */

	for(index = 0; index < NODE_ARRAY_LENGTH; index++) {
		NODE_SET_TYPE(nodes[index], node_type_unused);
		if(index < (NODE_ARRAY_LENGTH - 1)) {
			NODE_SET_NEXT(nodes[index], index + 1);
		} else {
			NODE_SET_NEXT(nodes[index], DELTA);
		}
	}

	node_alloc_next = 0;
	current_nodes_allocated = 0;
	node_high_watermark = 0;

} /* ast_init() */


/* node_new()
 *
 * in:     type  - type of new node
 * out:    nodes - allocates a new node in the global nodes array
 * return: index of new node
 *
 */

ast_index_t
node_new(node_type_t type) {

	ast_index_t n = node_alloc_next;

	if(n == DELTA) {
		NEWLINE;
		PUTS("Out of nodes.");
		NEWLINE;
		longjmp(restart_point, -1);
	}

	node_alloc_next = NODE_GET_NEXT(nodes[n]);
	current_nodes_allocated++;
	if(current_nodes_allocated > node_high_watermark) {
		node_high_watermark = current_nodes_allocated;
	}
	NODE_SET_TYPE(nodes[n], type);
	return n;

} /* node_new() */


/* node_free()
 *
 * in:     index - index of node to return to unused state
 * out:    nodes - marks node unused in global nodes array
 * return: nothing
 *
 */

void
node_free(ast_index_t index) {

	NODE_SET_TYPE(nodes[index], node_type_unused);
	NODE_SET_NEXT(nodes[index], node_alloc_next);
	node_alloc_next = index;
	current_nodes_allocated--;

} /* node_free() */


/* ast_print()
 *
 * in:     index - index of node to (recursively) print
 * out:    nothing
 * return: nothing
 *
 * Use this function to print formulas.  If you want to print the
 * entire formula in the nodes array (as opposed to just a
 * sub-formula), call with index 0.  Index 0 is always the root of the
 * formula tree.
 */

void
ast_print(ast_index_t index) {

	switch(NODE_GET_TYPE(nodes[index])) {
	case node_type_application:
		PUTCHAR('(');
		ast_print(NODE_GET_LEFT(nodes[index]));
		PUTCHAR(' ');
		ast_print(NODE_GET_RIGHT(nodes[index]));
		PUTCHAR(')');
		break;
	case node_type_abstraction:
		PUTCHAR('(');
		PUTCHAR('\\');
		PUTCHAR(NODE_GET_PARM(nodes[index]));
		PUTCHAR('.');
		ast_print(NODE_GET_BODY(nodes[index]));
		PUTCHAR(')');
		break;
	case node_type_variable:
	default:
		PUTCHAR(NODE_GET_VAR(nodes[index]));
		break;
	} /* switch on node type */

} /* ast_print() */


/* ast_report()
 *
 * in:     nothing
 * out:    nothing
 * return: nothing
 *
 * Reports statistics on AST node useage.
 *
 */

void
ast_report(void) {

	PUTS("Nodes used:");
	PUTCHAR('\t');
	PRINT_UINT(node_high_watermark);
	NEWLINE;

} /* ast_report() */


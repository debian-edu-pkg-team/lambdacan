#ifndef _LCI_PLATFORM_H_
#define _LCI_PLATFORM_H_

/* LambdaCan -- a Lambda-calculus Reducer for Microcontrollers
 * Copyright (c) 2005, 2008 Timothy Jon Fraser (tfraser@alum.wpi.edu)
 * 
 * This file is part of LambdaCan.
 *
 * LambdaCan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LambdaCan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LambdaCan.  If not, see <http://www.gnu.org/licenses/>.
 */

/* platform-specific stuff for POSIX */
#ifdef POSIX
#include <stdio.h>
#define PUTS(s) puts((s))
#define PUTCHAR(c) putchar((c))
#define PRINT_UINT(u) printf("%u", (u))
#define NEWLINE puts("");
inline static int
GETCHAR(void) {
	int key;
	key = getchar();
	if(key == '\n') {
		putchar('>');
		putchar('>');
		putchar(' ');
	}
	return key;
}
#endif

/* platform-specific stuff for ARDUINO */
#ifdef ARDUINO
#include <WProgram.h>
// #define PUTS(s) Serial.print((char *)(s)); Serial.print("\r\n")
#define PUTS(s) Serial.println((char *)(s));
#define PUTCHAR(c) Serial.print(c, BYTE)
#define PRINT_UINT(u) Serial.print(u, DEC)
#define NEWLINE Serial.println("")
inline int
GETCHAR(void) {
	int key;
	while(!Serial.available());
	key = Serial.read();
	Serial.print(key, BYTE);
	if(key == '\r') {
		Serial.print('\n', BYTE);
		Serial.print('>', BYTE);
		Serial.print('>', BYTE);
		Serial.print(' ', BYTE);
	}
	return key;
}
#endif

#endif
